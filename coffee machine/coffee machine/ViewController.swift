//
//  ViewController.swift
//  coffee machine
//
//  Created by Максим Бойко on 3/21/19.
//  Copyright © 2019 Максим Бойко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let coffemachine1 = CoffeMachine()
    @IBOutlet weak var CoffeeMachineField: UITextField!
   
    @IBAction func AddWater() {
        CoffeeMachineField.text = coffemachine1.AddWater()
    }
    
    @IBAction func AddCoffee() {
        CoffeeMachineField.text = coffemachine1.AddCoffee()
    }
    
    @IBAction func AddMilk() {
    CoffeeMachineField.text = coffemachine1.AddMilk()
}
    
    @IBAction func Americano() {
       CoffeeMachineField.text = coffemachine1.makeCofee(choice: "Americano")
    }
   
    @IBAction func Late() {
       CoffeeMachineField.text = coffemachine1.makeCofee(choice: "Late")
    }
    
    @IBAction func espresso() {
        CoffeeMachineField.text = coffemachine1.makeCofee(choice: "Espresso")
       
    }
    
   
    

    
}

