//
//  CoffeMachine.swift
//  coffee machine
//
//  Created by Максим Бойко on 3/23/19.
//  Copyright © 2019 Максим Бойко. All rights reserved.
//

import UIKit

class CoffeMachine: NSObject {
    var water = 0
    var coffee = 0
    var milk = 0
    
    func AddCoffee() -> String {
       var str = String()
        if coffee + 15 < 100 {
            coffee += 15
            str = "Bunker has \(coffee)g coffee"
        
        }
        else if coffee + 15 > 100{
           str = "Bunker has maximum coffee"
           
        }
         return str
}
    
    func AddWater() -> String {
        var str = String()
        if water + 200 < 1401 {
            water += 200
            str = "Bunker has \(water)ml water"
        }
        else if water + 200 >= 1401 {
            str = "Bunker has maximum water"
        }
        return str
    }
    
    func AddMilk() -> String {
       var str = String()
        if milk + 200 < 1401 {
            milk += 200
           str = "Bunker has \(milk)ml milk"
        }
        else if milk + 200 >= 1401 {
            str = "Bunker has maximum milk"
        }
        return str
    }
    
    func makeCofee(choice : String) -> String {
        var str = String()
        
        if (coffee>0) && (milk>0) && (water>0){
            str = "\(choice) is done"
            milk -= 200
            water -= 200
            coffee -= 15
            
        }
        else if coffee == 0 {
            str = "please add coffee"
            
        }
        else if milk == 0 {
            str = "please add milk"
        }
        else if water == 0 {
            str = "please add water"
        }
        return str
    }
    
}
